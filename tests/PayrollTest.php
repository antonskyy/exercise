<?php

use PHPUnit\Framework\TestCase;

final class PayrollTest extends TestCase
{
    public function testSalaryDay(): void
    {
        $this->assertEquals(
            '2020-01-31',
            Payroll::salaryDay(2020, 1)
        );

        $this->assertEquals(
            '2020-05-29',
            Payroll::salaryDay(2020, 5)
        );
    }

    public function testExpensesDay(): void
    {
        $this->assertEquals(
            '2020-01-01',
            Payroll::expensesDay(2020, 1, 1)
        );

        $this->assertEquals(
            '2020-02-17',
            Payroll::expensesDay(2020, 2, 15)
        );
    }
}