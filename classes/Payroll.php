<?php

class Payroll
{

  /**
   * Name of the file to write results to.
   *
   * @var string
   */
  protected $filename;

  /**
   * Year to produce results for.
   *
   * @var int
   */
  protected $year;

  /**
   * Creates a new instance.
   *
   * @param string $filename
   * @param int $year
   * @return void
   */
  public function __construct (string $filename, int $year = 0)
  {
    $this->filename = ($filename ? $filename : 'payroll') . '.csv'; // Must be a CSV file
    $this->year = $year ? $year : intval(date('Y')); // Defaults to the current year
  }

  /**
   * Writes results to a CSV file.
   *
   * @return void
   */
  public function writeToFile (): void
  {
    $csv = '';

    $headers = [
      'Month name',
      '1st expenses day',
      '2nd expenses day',
      'Salary day'
    ];

    // Build the header row
		foreach ($headers as $header) {
			$csv .= $this->wrapCell($header);
    }

    $csv .= PHP_EOL;

    // Build data rows
    for ($month = 1; $month <= 12; $month++) {
      $csv .=
        $this->wrapCell(date('F', mktime(0, 0, 0, $month, 1, $this->year))) . // Month name
        $this->wrapCell($this->expensesDay($this->year, $month, 1)) . // 1st expenses day
        $this->wrapCell($this->expensesDay($this->year, $month, 15)) . // 2nd expenses day
        $this->wrapCell($this->salaryDay($this->year, $month)) . // Salary day
        PHP_EOL
      ;
    }

    // Write results to file. There's a chance that the target file is locked
    // or otherwise unwritable so let's try to catch that and any other filesystem problems.
    try{
      if (file_put_contents('./' . $this->filename, $csv) !== false) {
        echo 'Successfully wrote results to ' . $this->filename;
      } else {
        throw new Exception('Unable to write results to ' . $this->filename . '. Make sure the file is not locked and try again.');
      }
    } catch (Exception $e) {
      echo 'Error: ' . $e->getMessage();
    }
  }

  /**
   * Calculates the salary date.
   *
   * @param int $year
   * @param int $month
   * @return string
   */
  public function salaryDay (int $year, int $month): string
  {
    $firstDayOfMonth = mktime(0, 0, 0, $month, 1, $year);
    $salaryDay = strtotime('last day of', $firstDayOfMonth);

    if (in_array(date('D', $salaryDay), ['Sat', 'Sun'])) {
      $salaryDay = strtotime('last friday of', $firstDayOfMonth);
    }

    return date('Y-m-d', $salaryDay);
  }

  /**
   * Calculates the expense payment date.
   *
   * @param int $year
   * @param int $month
   * @param int $day
   * @return string
   */
  public function expensesDay (int $year, int $month, int $day): string
  {
    $expensesDay =  mktime(0, 0, 0, $month, $day, $year);

    if (in_array(date('D', $expensesDay), ['Sat', 'Sun'])) {
      $expensesDay = strtotime('next monday', $expensesDay);
    }

    return date('Y-m-d', $expensesDay);
  }

  /**
   * Wraps up the supplied content as a CSV cell.
   *
   * @param string $content
   * @return string
   */
  public function wrapCell (string $content = ''): string
  {
    return '"' . $content . '",';
  }

}