<?php

require_once('./classes/Payroll.php');

$filename = '';
$year = 0;

// Prompt user for filename
$filename = readline('Enter filename (extension .csv will be added): ');

// Prompt user for year until a valid input is received
while ($year < 1900 || $year > 2100) { // Set reasonable range constraints
  $year = intval(readline('Enter year (must be between 1900-2100): '));
}

$payroll = new Payroll($filename, $year);
$payroll->writeToFile();
