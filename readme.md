## Coding Exercise: Payroll Dates

### Running the app

Navigate to the project directory and run the following:
`php app.php`
Output CSV file is generated in the project directory.

### Testing the app

Navigate to the project directory and run the following:
`composer update`
`./vendor/bin/phpunit --bootstrap ./vendor/autoload.php ./tests/PayrollTest.php`
